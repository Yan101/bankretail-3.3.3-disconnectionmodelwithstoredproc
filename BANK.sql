ALTER DATABASE [BANK] SET COMPATIBILITY_LEVEL = 90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BANK].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BANK] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [BANK] SET ANSI_NULLS OFF
GO
ALTER DATABASE [BANK] SET ANSI_PADDING OFF
GO
ALTER DATABASE [BANK] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [BANK] SET ARITHABORT OFF
GO
ALTER DATABASE [BANK] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [BANK] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [BANK] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [BANK] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [BANK] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [BANK] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [BANK] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [BANK] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [BANK] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [BANK] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [BANK] SET  ENABLE_BROKER
GO
ALTER DATABASE [BANK] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [BANK] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [BANK] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [BANK] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [BANK] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [BANK] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [BANK] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [BANK] SET  READ_WRITE
GO
ALTER DATABASE [BANK] SET RECOVERY FULL
GO
ALTER DATABASE [BANK] SET  MULTI_USER
GO
ALTER DATABASE [BANK] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [BANK] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'BANK', N'ON'
GO
USE [BANK]
GO
/****** Object:  Table [dbo].[Debitors]    Script Date: 11/28/2015 02:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Debitors](
	[ID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[PostNumber] [bigint] NOT NULL,
	[PhoneNumber] [nchar](50) NULL,
 CONSTRAINT [PK_Debitors] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'1db8b8a6-260b-4ad8-8327-0a0c9babaf3a', N'Тестовый Дебитор', 44534, N'453                                               ')
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'6786c9da-5a8f-469a-ad9e-10b045038286', N'11111111111111111111', 1, N'11111                                             ')
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'e6927cb7-9d99-439e-8cf1-16876ab912a1', N'qqq', 223, N'332                                               ')
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'0b742345-e756-4b1e-ad46-99bfd1cf2a19', N'22z', 22, N'22                                                ')
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'5f763363-b31a-4f33-aa96-a3a8937989de', N't', 4, N'4                                                 ')
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'a359913c-7963-45cb-89a2-ae25824b1070', N'Глеб Жиглов', 123, N'321                                               ')
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'a4af7b28-f6d0-4d1c-bb49-b3db9ee2e588', N'ю', 0, N'00                                                ')
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'f79e57b0-2862-4e63-9914-ca47059a5b94', N'1111', 11, N'111                                               ')
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'77aa6270-cf9b-4f53-adb5-cf9dda3288a3', N'222', 222, N'222                                               ')
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'd6fc541d-ef0d-480b-94a0-e7dc89b0e40b', N'555', 555, N'555                                               ')
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'f7ed0050-5d5e-4b56-8167-eeb7d30e5ff4', N'33233', 332, N'332                                               ')
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'e2eeeeee-eeee-eeee-eeee-eeeeeeeeeee2', N'Петренко Анна Александровна', 32424, N'003 8 093 345-78-02                               ')
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'e3eeeeee-eeee-eeee-eeee-eeeeeeeeeee4', N'Радужный Алексей Иванович', 32454654, N'33                                                ')
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'e44eeeee-eeee-eeee-eeee-eeeeeeeeeeee', N'Гудок Александр Сергеевич', 3242345, N'003 8 067 895-96-03                               ')
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'd19619dd-864c-4b55-b1d1-f3451f5e6325', N'z3ок', 98739, NULL)
INSERT [dbo].[Debitors] ([ID], [Name], [PostNumber], [PhoneNumber]) VALUES (N'd19619dd-864c-4b55-b1d1-f3651f5e6325', N'z2', 133323, N'                                                  ')
/****** Object:  Table [dbo].[Credits]    Script Date: 11/28/2015 02:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Credits](
	[ID] [uniqueidentifier] NOT NULL,
	[DebitorID] [uniqueidentifier] NOT NULL,
	[Amount] [money] NOT NULL,
	[Balance] [money] NOT NULL,
	[OpenDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Credits] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'1f593997-0d15-4d88-b842-0b7623c8302e', N'e6927cb7-9d99-439e-8cf1-16876ab912a1', 1000.0000, 900.0000, CAST(0x0000A55C00032BBD AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'64e21b1d-310b-4a32-bd14-10a28b458f33', N'e3eeeeee-eeee-eeee-eeee-eeeeeeeeeee4', 100000.0000, 100000.0000, CAST(0x0000A532016036E5 AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'c4a1fe70-dfbe-4792-86fb-14464e7e517b', N'd6fc541d-ef0d-480b-94a0-e7dc89b0e40b', 5555.0000, 4445.0000, CAST(0x0000A55D003BAE98 AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'99d627ae-3d47-4428-b1bc-184ca69f524e', N'77aa6270-cf9b-4f53-adb5-cf9dda3288a3', 5555.0000, 4550.0000, CAST(0x0000A55C016BCCD5 AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'9f75fdd2-9436-4019-850f-280a72194c36', N'a359913c-7963-45cb-89a2-ae25824b1070', 1000.0000, 397.0000, CAST(0x0000A55C00853E46 AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'6c27df9e-3579-41c2-9e7b-2e8c69cea52f', N'f79e57b0-2862-4e63-9914-ca47059a5b94', 1000.0000, 1000.0000, CAST(0x0000A55C016AB696 AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'c1539039-9e62-433f-8fe9-5fa350d01fbb', N'0b742345-e756-4b1e-ad46-99bfd1cf2a19', 2222.0000, 2000.0000, CAST(0x0000A55D0004653B AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'fd78a28a-dec1-4b31-add5-6455212c9735', N'd19619dd-864c-4b55-b1d1-f3651f5e6325', 150000.0000, 149556.0000, CAST(0x0000A5350130FFA3 AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'366d8184-0ad8-4410-b2e8-83e4e9a108c0', N'f79e57b0-2862-4e63-9914-ca47059a5b94', 1234.0000, 1234.0000, CAST(0x0000A55D00429E23 AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'd3ffdc18-5d05-4c18-9c4b-88f6c93751c0', N'e3eeeeee-eeee-eeee-eeee-eeeeeeeeeee4', 5000.0000, 5000.0000, CAST(0x0000A5320160D08C AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'40548062-71b7-450d-a6c3-b03788fc465d', N'f7ed0050-5d5e-4b56-8167-eeb7d30e5ff4', 1333.0000, 1001.0000, CAST(0x0000A55D0051BA8D AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'c4c2d9a6-eed0-425d-b211-bdf3764b9011', N'e44eeeee-eeee-eeee-eeee-eeeeeeeeeeee', 19999.0000, 19444.0000, CAST(0x0000A52D0019D73C AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'360abd89-bbaa-4475-999c-bef839a3ed08', N'e44eeeee-eeee-eeee-eeee-eeeeeeeeeeee', 16666.0000, 16666.0000, CAST(0x0000A56A0019E8D0 AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'f892032b-b4c2-42b9-94f7-d62b6c78796b', N'e44eeeee-eeee-eeee-eeee-eeeeeeeeeeee', 1666.0000, 1666.0000, CAST(0x0000A55B00CA0B57 AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'ee87e528-f758-490c-96a3-e3a90d54a9d0', N'6786c9da-5a8f-469a-ad9e-10b045038286', 1111.0000, 1011.0000, CAST(0x0000A55D003BDE4C AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'6823f8cf-7d18-491b-a89b-e8b6ecf3f271', N'a4af7b28-f6d0-4d1c-bb49-b3db9ee2e588', 1000.0000, 800.0000, CAST(0x0000A55B00C7BF2B AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'3e1d8aa4-1843-49f6-9b6b-f2407dae978e', N'e44eeeee-eeee-eeee-eeee-eeeeeeeeeeee', 166666.0000, 166666.0000, CAST(0x0000A52D001A2110 AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'99aa645e-cefa-430a-a322-f4e4c12b2ca6', N'e44eeeee-eeee-eeee-eeee-eeeeeeeeeeee', 150000.0000, 149850.0000, CAST(0x0000A41C00188CC4 AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'99aa645e-cefa-430a-a322-f4e4c12b2ca7', N'e44eeeee-eeee-eeee-eeee-eeeeeeeeeeee', 1200000000.0000, 1199973689.0000, CAST(0x0000A41C00000000 AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'99aa645e-cefa-430a-a322-f4e4c12b2ca8', N'e44eeeee-eeee-eeee-eeee-eeeeeeeeeeee', 160000.0000, 160000.0000, CAST(0x0000A41C00000000 AS DateTime))
INSERT [dbo].[Credits] ([ID], [DebitorID], [Amount], [Balance], [OpenDate]) VALUES (N'99aa645e-cefa-430a-a322-f4e4c12b2ca9', N'e44eeeee-eeee-eeee-eeee-eeeeeeeeeeee', 156000.0000, 156000.0000, CAST(0x0000A42100000000 AS DateTime))
/****** Object:  StoredProcedure [dbo].[SearchDebitor]    Script Date: 11/28/2015 02:29:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SearchDebitor]
(
		@debName nvarchar(50),
		@debPostNum nvarchar(50),
		@debPhoneNum nvarchar(50)
	)
AS

	SELECT * FROM Debitors WHERE
	Name like '%' + @debName + '%' AND
	CAST (PostNumber as nvarchar(50)) like '%' + @debPostNum + '%' AND
	(PhoneNumber like '%' + @debPhoneNum + '%' OR PhoneNumber IS NULL)
 
 RETURN
GO
/****** Object:  Table [dbo].[Payments]    Script Date: 11/28/2015 02:29:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payments](
	[ID] [uniqueidentifier] NOT NULL,
	[CreditsID] [uniqueidentifier] NOT NULL,
	[Amount] [decimal](9, 2) NOT NULL,
	[PaymentDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Payments] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'e13aa0a9-e0a6-46b4-9453-09ab800a3610', N'99aa645e-cefa-430a-a322-f4e4c12b2ca7', CAST(100.00 AS Decimal(9, 2)), CAST(0x0000A55C016AEB1B AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'3a986094-4287-4e1d-af5c-1532a8e1b26a', N'9f75fdd2-9436-4019-850f-280a72194c36', CAST(120.00 AS Decimal(9, 2)), CAST(0x0000A55C0086F100 AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'abc7c42d-a1b5-4fde-bbb3-1a165cdeac22', N'c1539039-9e62-433f-8fe9-5fa350d01fbb', CAST(222.00 AS Decimal(9, 2)), CAST(0x0000A55D00046DB6 AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'458458a9-5b8f-444d-9ba9-251454b82251', N'c4a1fe70-dfbe-4792-86fb-14464e7e517b', CAST(555.00 AS Decimal(9, 2)), CAST(0x0000A55D003BB919 AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'cf515fba-72e0-4eb8-9147-260eda574a8d', N'ee87e528-f758-490c-96a3-e3a90d54a9d0', CAST(100.00 AS Decimal(9, 2)), CAST(0x0000A55D003BE7EB AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'9287eef6-4f4f-457e-8e15-26ba12cee111', N'9f75fdd2-9436-4019-850f-280a72194c36', CAST(150.00 AS Decimal(9, 2)), CAST(0x0000A55C008619CE AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'b5b49736-21a4-41bd-b7fc-439cf7628725', N'c4a1fe70-dfbe-4792-86fb-14464e7e517b', CAST(555.00 AS Decimal(9, 2)), CAST(0x0000A55D003C340D AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'1d2da17e-067e-406c-9656-6a37a2f3facc', N'9f75fdd2-9436-4019-850f-280a72194c36', CAST(222.00 AS Decimal(9, 2)), CAST(0x0000A55C00862CFA AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'26ca8085-f538-4493-ab48-6b4816941c6b', N'6823f8cf-7d18-491b-a89b-e8b6ecf3f271', CAST(200.00 AS Decimal(9, 2)), CAST(0x0000A55D00A3FC60 AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'431150eb-caec-4898-863d-714a3d5d40cb', N'99aa645e-cefa-430a-a322-f4e4c12b2ca6', CAST(150.00 AS Decimal(9, 2)), CAST(0x0000A4B4017E830C AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'51da54e7-84bd-4a0e-be3f-7cd70da2a5ed', N'1f593997-0d15-4d88-b842-0b7623c8302e', CAST(100.00 AS Decimal(9, 2)), CAST(0x0000A55C000D5604 AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'af625a77-99bb-4ce6-b4df-83b08b468361', N'fd78a28a-dec1-4b31-add5-6455212c9735', CAST(444.00 AS Decimal(9, 2)), CAST(0x0000A4B30131508C AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'4a9488be-b6f5-40c1-91e5-92d61aa40c65', N'99d627ae-3d47-4428-b1bc-184ca69f524e', CAST(450.00 AS Decimal(9, 2)), CAST(0x0000A55C016C36C8 AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'0e292041-3938-4520-8960-999eb13b2ff5', N'9f75fdd2-9436-4019-850f-280a72194c36', CAST(111.00 AS Decimal(9, 2)), CAST(0x0000A55C0086CFEC AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'95bd6871-968a-46c4-b5e6-a161fc53dfba', N'99aa645e-cefa-430a-a322-f4e4c12b2ca7', CAST(1000.00 AS Decimal(9, 2)), CAST(0x0000A55C00731B09 AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'4eb861b7-4b65-44e1-b789-b9ff5f3ca347', N'99aa645e-cefa-430a-a322-f4e4c12b2ca7', CAST(25000.00 AS Decimal(9, 2)), CAST(0x0000A55C00733382 AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'95d9e22a-9b88-4810-b048-bf7c9e98a68e', N'40548062-71b7-450d-a6c3-b03788fc465d', CAST(332.00 AS Decimal(9, 2)), CAST(0x0000A55D0051CB3C AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'1fc593c1-c295-4b8e-8a7b-c1cdde1d7033', N'99aa645e-cefa-430a-a322-f4e4c12b2ca7', CAST(100.00 AS Decimal(9, 2)), CAST(0x0000A55C016AC0C3 AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'8b634519-a8a6-4a26-b614-d24f9b027ea1', N'99aa645e-cefa-430a-a322-f4e4c12b2ca7', CAST(150.00 AS Decimal(9, 2)), CAST(0x0000A55C00041C79 AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'cdb80b49-657b-4398-9697-db7724b32173', N'99aa645e-cefa-430a-a322-f4e4c12b2ca7', CAST(111.00 AS Decimal(9, 2)), CAST(0x0000A55C016B40C5 AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'047bd24b-286f-4669-858b-e36c041aae31', N'99d627ae-3d47-4428-b1bc-184ca69f524e', CAST(555.00 AS Decimal(9, 2)), CAST(0x0000A55C016BD7DC AS DateTime))
INSERT [dbo].[Payments] ([ID], [CreditsID], [Amount], [PaymentDate]) VALUES (N'3c78ffe1-e93b-40f8-bc5e-e8d625427017', N'c4c2d9a6-eed0-425d-b211-bdf3764b9011', CAST(555.00 AS Decimal(9, 2)), CAST(0x0000A52D014F2CB0 AS DateTime))
/****** Object:  StoredProcedure [dbo].[GetAllCreditsByCurrentDebitor]    Script Date: 11/28/2015 02:29:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllCreditsByCurrentDebitor]
	(
		@debitorID uniqueidentifier
	)
AS

	SELECT * FROM Credits Where DebitorID = @debitorID Order By OpenDate

	RETURN
GO
/****** Object:  StoredProcedure [dbo].[SavePayment]    Script Date: 11/28/2015 02:29:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SavePayment]
(
	@ID uniqueidentifier, 
	@CreditID uniqueidentifier, 
	@Amount decimal, 
	@PaymentDate DateTime
)
AS
	INSERT INTO Payments (Payments.ID, Payments.CreditsID, Payments.Amount, Payments.PaymentDate)
		VALUES (@ID, @CreditID, @Amount, @PaymentDate);
    UPDATE Credits SET Credits.Balance = (Credits.Balance - @Amount)
		WHERE Credits.ID = @CreditID;
RETURN
GO
/****** Object:  ForeignKey [FK_Credits_Debitors]    Script Date: 11/28/2015 02:29:28 ******/
ALTER TABLE [dbo].[Credits]  WITH CHECK ADD  CONSTRAINT [FK_Credits_Debitors] FOREIGN KEY([DebitorID])
REFERENCES [dbo].[Debitors] ([ID])
GO
ALTER TABLE [dbo].[Credits] CHECK CONSTRAINT [FK_Credits_Debitors]
GO
/****** Object:  ForeignKey [FK_Payments_Credits]    Script Date: 11/28/2015 02:29:29 ******/
ALTER TABLE [dbo].[Payments]  WITH CHECK ADD  CONSTRAINT [FK_Payments_Credits] FOREIGN KEY([CreditsID])
REFERENCES [dbo].[Credits] ([ID])
GO
ALTER TABLE [dbo].[Payments] CHECK CONSTRAINT [FK_Payments_Credits]
GO

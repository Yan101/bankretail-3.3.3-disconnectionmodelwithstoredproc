﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Collections;

namespace BANKretail
{
    public partial class NewCredit : Form
    {
        public Guid DebID
        {
            get { return new Guid(ltbx_debitorID.SelectedValue.ToString()); }
        }
        public Guid CredID
        {
            get { return new Guid(txbx_creditID.Text); }
        }
        public Int64 CredAmount
        {
            get { return Int64.Parse(txbx_creditAmount.Text.Trim()); }
        }
        public Int64 CredBalance
        {
            get { return Int64.Parse(txbx_creditBalance.Text.Trim()); }
        }
        public DateTime OpenDate
        {
            get { return dtp_creditOpenDate.Value; }
        }
        public NewCredit(DataSet bankDataSet)
        {
            InitializeComponent();

            txbx_creditID.Text = Guid.NewGuid().ToString();
            //Запоняем листбоксы!
            ltbx_debitorID.DataSource = bankDataSet.Tables["debitorTable"];
            ltbx_debitorName.DataSource = bankDataSet.Tables["debitorTable"];
        }

        private void btn_saveNewCredit_Click(object sender, EventArgs e)
        {
            if (txbx_creditBalance.Text != string.Empty)
                DialogResult = DialogResult.OK;
            else
                DialogResult = DialogResult.No;
        }

        private void txbx_creditAmount_Leave(object sender, EventArgs e)
        {
            if (txbx_creditAmount.Text == string.Empty ||
                Int64.Parse(txbx_creditAmount.Text.Trim()) < 100 || //Parse - когда уверенны что вводим сразу числа!
                Int64.Parse(txbx_creditAmount.Text.Trim()) > 100000000)
            {
                lbl_messageCreditAmount.Text = "Недопустимое значение суммы кредита!";
                lbl_messageCreditAmount.ForeColor = Color.Red;
                btn_saveNewCredit.Enabled = false;
            }
            else
            {
                lbl_messageCreditAmount.Text = "Сумма допустима.";
                lbl_messageCreditAmount.ForeColor = Color.Green;
                btn_saveNewCredit.Enabled = true;
            }
        }

        private void txbx_creditAmount_TextChanged(object sender, EventArgs e)
        {
            txbx_creditAmount.Text = txbx_creditAmount.Text.Trim().Replace(" ", "");
            txbx_creditBalance.Text = txbx_creditAmount.Text;
        }
    }
}

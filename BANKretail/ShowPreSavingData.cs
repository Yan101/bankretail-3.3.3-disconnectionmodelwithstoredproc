﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BANKretail
{
    public partial class ShowPreSavingData : Form
    {
        DataSet ds; //1
        public ShowPreSavingData(DataSet ds)
        {
            InitializeComponent();
            this.ds = ds;
            this.Load += new EventHandler(ShowPreSavingData_Load);//2 Полная загрузка формы
        }
        void ShowPreSavingData_Load(object sender, EventArgs e)
        {
            #region ОТСЛЕЖИВАЕМ ИЗМЕНЕННЫЕ ПОЛЯ
            /*
            DataView debitorDV = new DataView(ds.Tables[0], "", "Name", DataViewRowState.Added);//3 new DataView(Дебиторы, БезФильтра, СортировкапоName, ТолькоДобавленныеСтроки)
            DataView creditDV = new DataView(ds.Tables[1], "", "Balance", DataViewRowState.Added);
            DataView paymentsDV = new DataView(ds.Tables[2], "", "Amount", DataViewRowState.Added);
            
            if (creditDV.Count == 0 && debitorDV.Count == 0 && paymentsDV.Count == 0)
            {
                MessageBox.Show("Нет данных для сохранения");
                DialogResult = DialogResult.No;
                return;
            }
            dgv_debitors.DataSource = debitorDV; //4 привязать DGV и View
            dgv_credits.DataSource = creditDV;
            dgv_payments.DataSource = paymentsDV;
            */
            #endregion
            if (ds == null)
            {
                MessageBox.Show("Нет данных для сохранения");
                DialogResult = DialogResult.No;
                return;
            }
            
            dgv_debitors.DataSource = ds.Tables[0]; //Полученный отсортирвоанный DataSet через GEtChanges
            dgv_credits.DataSource = ds.Tables[1];
            dgv_payments.DataSource = ds.Tables[2];
        }
        private void btn_SaveData_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
        private void btn_deferSaving_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.No;
        }
        private void btn_rejectChanges_Click(object sender, EventArgs e)
        {
            ds.RejectChanges(); //Откат изменений
            DialogResult = DialogResult.No;
        }
    }
}

﻿namespace BANKretail
{
    partial class ShowPreSavingData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_debitors = new System.Windows.Forms.DataGridView();
            this.dgv_credits = new System.Windows.Forms.DataGridView();
            this.dgv_payments = new System.Windows.Forms.DataGridView();
            this.btn_SaveData = new System.Windows.Forms.Button();
            this.btn_deferSaving = new System.Windows.Forms.Button();
            this.btn_rejectChanges = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_debitors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_credits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_payments)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_debitors
            // 
            this.dgv_debitors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_debitors.Location = new System.Drawing.Point(12, 12);
            this.dgv_debitors.Name = "dgv_debitors";
            this.dgv_debitors.Size = new System.Drawing.Size(744, 150);
            this.dgv_debitors.TabIndex = 0;
            // 
            // dgv_credits
            // 
            this.dgv_credits.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_credits.Location = new System.Drawing.Point(12, 167);
            this.dgv_credits.Name = "dgv_credits";
            this.dgv_credits.Size = new System.Drawing.Size(744, 150);
            this.dgv_credits.TabIndex = 1;
            // 
            // dgv_payments
            // 
            this.dgv_payments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_payments.Location = new System.Drawing.Point(12, 323);
            this.dgv_payments.Name = "dgv_payments";
            this.dgv_payments.Size = new System.Drawing.Size(744, 150);
            this.dgv_payments.TabIndex = 2;
            // 
            // btn_SaveData
            // 
            this.btn_SaveData.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_SaveData.Location = new System.Drawing.Point(12, 483);
            this.btn_SaveData.Name = "btn_SaveData";
            this.btn_SaveData.Size = new System.Drawing.Size(247, 23);
            this.btn_SaveData.TabIndex = 3;
            this.btn_SaveData.Text = "Сохранить";
            this.btn_SaveData.UseVisualStyleBackColor = false;
            this.btn_SaveData.Click += new System.EventHandler(this.btn_SaveData_Click);
            // 
            // btn_deferSaving
            // 
            this.btn_deferSaving.Location = new System.Drawing.Point(265, 483);
            this.btn_deferSaving.Name = "btn_deferSaving";
            this.btn_deferSaving.Size = new System.Drawing.Size(246, 23);
            this.btn_deferSaving.TabIndex = 4;
            this.btn_deferSaving.Text = "Временно Отложить Сохр";
            this.btn_deferSaving.UseVisualStyleBackColor = true;
            this.btn_deferSaving.Click += new System.EventHandler(this.btn_deferSaving_Click);
            // 
            // btn_rejectChanges
            // 
            this.btn_rejectChanges.Location = new System.Drawing.Point(517, 483);
            this.btn_rejectChanges.Name = "btn_rejectChanges";
            this.btn_rejectChanges.Size = new System.Drawing.Size(239, 23);
            this.btn_rejectChanges.TabIndex = 5;
            this.btn_rejectChanges.Text = "Отменить изменения";
            this.btn_rejectChanges.UseVisualStyleBackColor = true;
            this.btn_rejectChanges.Click += new System.EventHandler(this.btn_rejectChanges_Click);
            // 
            // ShowPreSavingData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 518);
            this.Controls.Add(this.btn_rejectChanges);
            this.Controls.Add(this.btn_deferSaving);
            this.Controls.Add(this.btn_SaveData);
            this.Controls.Add(this.dgv_payments);
            this.Controls.Add(this.dgv_credits);
            this.Controls.Add(this.dgv_debitors);
            this.Name = "ShowPreSavingData";
            this.Text = "ShowPreSavingData";
            this.Load += new System.EventHandler(this.ShowPreSavingData_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_debitors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_credits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_payments)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_debitors;
        private System.Windows.Forms.DataGridView dgv_credits;
        private System.Windows.Forms.DataGridView dgv_payments;
        private System.Windows.Forms.Button btn_SaveData;
        private System.Windows.Forms.Button btn_deferSaving;
        private System.Windows.Forms.Button btn_rejectChanges;
    }
}
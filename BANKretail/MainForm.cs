﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Collections;

namespace BANKretail
{
    public partial class MainForm : Form
    {
        DataSet bankDataSet;
        DAL dal = new DAL();
        //#3.25 DataView - для отображения одного дебитора и всех его кредитов. Связь трех таблиц между собой
        DataView debitorDV, creditDV, paymentDV;
        public MainForm() //Уровни Presentable и BLL
        {
            InitializeComponent();

            FillbankDataSet(); //Заполняет DataSet
            debitorDV = new DataView(bankDataSet.Tables["debitorTable"]);
            creditDV = new DataView(bankDataSet.Tables["creditTable"]);
            paymentDV = new DataView(bankDataSet.Tables["paymentTable"]);

            BindingGridView(); //Привязывает каждый GridView к таблицам.
            SettingsDGV_Debitors();
        }
        private void FillbankDataSet()
        {
            bankDataSet = dal.GetDBData();
        }
        private void BindingGridView()
        {
            if (bankDataSet.Tables.Count > 0)
            {
                dgv_debitors.DataSource = debitorDV; //= bankDataSet.Tables["debitorTable"];
                dgv_credits.DataSource = creditDV;
                dgv_payments.DataSource = paymentDV;

            }
        }
        //#1 Скрываем из DGV_Debitors лишние колонки!
        void SettingsDGV_Debitors()
        {
            try //Если ошибка в названии колонки, то ввывести все колонки
            {
                //Обращаемся к колонкам DGV по индексу
                dgv_debitors.Columns["ID"].Visible = false; ////dgv_debitors.Columns[0].Visible = false;
                dgv_debitors.Columns["PostNumber"].Visible = false;
                dgv_debitors.Columns["PhoneNumber"].Visible = false;

                dgv_debitors.TopLeftHeaderCell.Value = "#"; //Установили знак Ячейке верхней левой!
            }
            catch (Exception)
            {

            }
        }
        private void MainForm_Load(object sender, EventArgs e) //dgv_debitors.CellEnter += !!!ТЕПЕРЬ Нажать TAB TAB
        {
            //#11 Дублирование инфы в TEXTBOX!
            dgv_debitors.CellEnter += new DataGridViewCellEventHandler(dgv_debitors_CellEnter);
            dgv_credits.CellEnter += new DataGridViewCellEventHandler(dgv_credits_CellEnter);
        }
        void dgv_credits_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            //#2 
            if (e == null)
            {
                dgv_payments.DataSource = null;
                return; //если пусто тогда не нужно устанавливать #1 -RowFilter!
            }
            //#1 Фильтр для DGV credits
            paymentDV.RowFilter = string.Format("CreditsID = '{0}'", 
                dgv_credits.Rows[e.RowIndex].Cells["ID"].FormattedValue.ToString());

            //#2 Если не получили записей, то payment - сбросить. ПЕРЕПРИВЯЗКА, т.к. dgv_payments.DataSource = null;
            dgv_payments.DataSource = paymentDV;
        }
        void dgv_debitors_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            //#11 Дублирование инфы в TEXTBOX!
            txbx_debitorID.Text = dgv_debitors.CurrentRow.Cells["ID"].Value.ToString();
            txbx_debitorName.Text = dgv_debitors.CurrentRow.Cells["Name"].Value.ToString();
            txbx_debitorPostNumber.Text = dgv_debitors.CurrentRow.Cells["PostNumber"].Value.ToString();
            txbx_debitorPhoneNumber.Text = dgv_debitors.CurrentRow.Cells["PhoneNumber"].Value.ToString();

            //#1 Фильтр для DGV debitors  Отбор полей по значению.  dgv_debitors[поле=ID, та строка которая иниц событие]
            creditDV.RowFilter = string.Format("DebitorID = '{0}'", dgv_debitors[0, e.RowIndex].FormattedValue.ToString());

            //#2 Если не получили записей, то payment - сбросить
            if (creditDV.Count == 0)
                dgv_credits_CellEnter(null, null); //Вызывать обработчика событий без события - вручную, а не через возникновение события
        }
        private void addNewDebitorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //ОТкрытие формы. прием текстовых полей и передача на DAL, для сохранения.
            NewDebitor newDebitor = new NewDebitor();
            if (newDebitor.ShowDialog() != DialogResult.OK)
            {
                MessageBox.Show("Дебитор не создан");
                return;
            }

            bool result = true;
            //Вытянуть открытые поля, которые предоставляют дебитора и сохранить их.
            DataRow newDeb_dr = bankDataSet.Tables["debitorTable"].NewRow(); //NewRow - новая строка для таблицы
            try
            {
                newDeb_dr["ID"] = newDebitor.DebID;
                newDeb_dr["Name"] = newDebitor.DebName;
                newDeb_dr["PostNumber"] = newDebitor.DebPostNum;
                newDeb_dr["PhoneNumber"] = newDebitor.DebPhoneNum;

                bankDataSet.Tables["debitorTable"].Rows.Add(newDeb_dr);  //Добавляем строку в DataSet

                //Save data
                /*         if (!dal.SaveChanges(bankDataSet)) //Если не успешно - false
                              result = false;
                 */
                //FillbankDataSet();
            }
            catch (Exception)
            {
                MessageBox.Show("Неверно указаны поля");
                result = false;
            }
            finally
            {
                //Если в try - не сможем сохранить данные и не вылетит Ошибка,то этот блок всегда выполнится.
                if (result)
                    MessageBox.Show("Дебитор успешно создан");
                else
                    MessageBox.Show("Дебитор не создан");
            }
        }
        private void openNewCreditToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewCredit newCredit = new NewCredit(this.bankDataSet);
            if (newCredit.ShowDialog() != DialogResult.OK)
            {
                MessageBox.Show("Кредит не выдан");
                return;
            }
            bool result = true;
            //Вытянуть открытые поля, которые предоставляют дебитора и сохранить их.
            DataRow newCred_dr = bankDataSet.Tables["creditTable"].NewRow(); //NewRow - новая строка для таблицы
            try
            {
                newCred_dr["ID"] = newCredit.CredID;
                newCred_dr["DebitorID"] = newCredit.DebID;
                newCred_dr["Amount"] = newCredit.CredAmount;
                newCred_dr["Balance"] = newCredit.CredBalance;
                newCred_dr["OpenDate"] = newCredit.OpenDate;
                bankDataSet.Tables["creditTable"].Rows.Add(newCred_dr);  //Добавляем строку в DataSet

                //Save data
                /*           if (!dal.SaveChanges(bankDataSet)) //Если не успешно - false
                               result = false;
                 */
                 //FillbankDataSet();
            }
            catch (Exception)
            {
                MessageBox.Show("Неверно указаны поля");
                result = false;
            }
            finally
            {
                //Если в try - не сможем сохранить данные и не вылетит Ошибка,то этот блок всегда выполнится.
                if (result)
                    MessageBox.Show("Кредит успешно выдан");
                else
                    MessageBox.Show("Кредит не выдан");
            }
        }
        private void passNewPaymentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Просто отобразить форму
            NewPayment newPayment = new NewPayment(bankDataSet);
            //#7 NewPayment Принять состояние формы
            if (newPayment.ShowDialog() != DialogResult.OK)
            {
                MessageBox.Show("Платеж не принят");
                return;
            }
            bool result = true;
            //#8 Реализовать новую строку из таблицы Payments
            DataRow newPayment_dr = bankDataSet.Tables["paymentTable"].NewRow();
            try
            {
                //Заполнить все поля каждой колонки для НОВОЙ строки newPayment_dr
                newPayment_dr["ID"] = newPayment.PaymentID;
                newPayment_dr["CreditsID"] = newPayment.CreditID;
                newPayment_dr["Amount"] = newPayment.PaymentAmount;
                newPayment_dr["PaymentDate"] = newPayment.PaymentDate;

                //#8.1 Добавить ее в таблицу Payments
                bankDataSet.Tables["paymentTable"].Rows.Add(newPayment_dr);

                #region #8.1.1 Производим поиск соответствующей строки в РОДИТЕЛЬСКоЙ таблице! другим способом! Find() - принимает первичный Ключ (их может быть несколько)
                //Найти строку которую нужно обновить в таблице creditsTable. (DAL - GetDBData - Связь CreditPayment). GetParentRow - вернет соответствующую запись из родительской таблицы
                //DataRow creditTable_dr = newPayment_dr.GetParentRow("CreditPayment");
                            //ИЛИ
                //DataRow creditTable_dr = bankDataSet.Tables["creditTable"].Rows.Find(newPayment.CreditID);
                
                //Получили родительскую строку и вносим теперь изменения
                /* creditTable_dr.BeginEdit();
                creditTable_dr["Balance"] = decimal.Parse(creditTable_dr["Balance"].ToString()) - newPayment.PaymentAmount; //Одновременно меняем табличку Credits - Balance. Вычитаем внесенный платеж от всей суммы.
                creditTable_dr.EndEdit();*/
                #endregion

                //#8.1.2 СЛОЖНЫЙ АДАПТЕР!!! При изменении данныхв одной таблице, нужно следить за Родит и Дочерними таблицами; адаптер отслеживает изменения.

                result = true;

                //#8.2 Вызвать метод SaveChanges, передать ему в качестве параметра  DataSet с новыми строками.
                //Выполнится для 2 таблиц creditDA.Update(bankDataSet_.Tables["creditTable"]); paymentDA.Update(bankDataSet_.Tables["paymentTable"]); 
  /*              if (!dal.SaveChanges(bankDataSet))
                    result = false;
   */
                //Занесли данные в DAL. Нужно обновить данные
                FillbankDataSet();
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Неверно указаные некоторые реквизиты платежа");
                result = false;
            }
            catch (InvalidConstraintException)
            {
                MessageBox.Show("Невозмоно принять платеж по данному кредиту");
                result = false;
            }
            finally
            {
                if (result) 
                    MessageBox.Show("Платеж зачислен");
                else
                    MessageBox.Show("Платеж не зачислен");
            }
        }
        private void showSaveDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //ShowPreSavingData showNewData = new ShowPreSavingData(bankDataSet);
            //Возвращается DataSet в котором только измененные данные!
            ShowPreSavingData showNewData = new ShowPreSavingData(bankDataSet.GetChanges()); //GetChanges() АНАЛОГ измененных полей - ПРОЩЕ. GetChanges(DataRowState.Added)
            if (showNewData.ShowDialog() == DialogResult.OK)
            {
                dal.SaveChanges(bankDataSet);
                FillbankDataSet(); //перезалить БД. Динамически отобразятся изменения в DGV. Чтобы Баланс уменишился на кол-во платежа
            }
        }
        private void btn_search_Click(object sender, EventArgs e)
        {
            string filter = String.Format("Name Like '%{0}%' AND " +
                "Convert([PostNumber], 'System.String') Like '%{1}%' AND " +
                "PhoneNumber Like '%{2}%'",
                txbx_searchedDebName.Text.Trim(), 
                txbx_searchedDebPostNumber.Text.Trim(),
                txbx_searchedDebPhoneNumber.Text.Trim());
            //#1 Применяем созданный фильтр в Вьюшке
            debitorDV.RowFilter = filter;

            if (debitorDV.Count > 0)
            {
                //#2 Загрузить выбранные записи в DGV
                dgv_debitors.DataSource = debitorDV;
                dgv_credits.DataSource = creditDV;
                dgv_payments.DataSource = paymentDV;
                MessageBox.Show("Найдено " + debitorDV.Count.ToString() + " дебиторов", debitorDV.Count.ToString() + " дебиторов");
            }
            else
            {
                MessageBox.Show("Ничего не Найдено");
                txbx_debitorID.Text = "";
                txbx_debitorName.Text = "";
                txbx_debitorPostNumber.Text = "";
                txbx_debitorPhoneNumber.Text = "";
                
                //dgv_debitors.DataSource = null;
                dgv_credits.DataSource = null;
                dgv_payments.DataSource = null;
            }
        }
        private void saveDataToCSVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Сохранять после внесенных изменений!
            if (dal.SaveAllData(bankDataSet))
                MessageBox.Show("Все данные успешно сохраненны");
            else 
                MessageBox.Show("Данные не сохранены");
        }
    }
}
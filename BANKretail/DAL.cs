﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;      
using System.Data.SqlClient; //DataAdapter   
using System.Data.Common;     
using System.Globalization; 
using System.IO; 

using System.Configuration; //AddReference - ConfurationManager  
using System.Data; //DataSet

namespace BANKretail
{
    //------------------СОЗДАНИЕ АДАПТЕРА---------------------
    public class DAL
    {
        //#1 connectionString
        string connectionString = ConfigurationManager.ConnectionStrings["BANKmanagerConnectionString"].ConnectionString;

        //#3 DataAdapter. В консрукторе DAL - создадим обьекты для debitorDA, creditDA, paymentDA;
        SqlDataAdapter debitorDA, creditDA, paymentDA;
        SqlCommandBuilder debitorCB, creditCB, paymentCB; //#3.29 Аналог DataAdapter

        //#2 DataSet
        DataSet bankDataSet;
        public DAL()
        {
            bankDataSet = new DataSet("BankData"); 
            //#Запрос
            debitorDA = new SqlDataAdapter("SELECT * FROM Debitors ORDER BY Name", connectionString); //SqlDataAdapter(Текст выборки из БД, соединение);
            #region #3.28 Расширенная настройка DataAdapter руками
            /*  //#1 Объявили InsertCommand и назначсили ему новый объект SqlCommand
                debitorDA.InsertCommand = new SqlCommand();
                
                //#2 Настройка SqlCommand
                //Подключение
                debitorDA.InsertCommand.Connection = new SqlConnection(connectionString); //Вставка новых записей в БД, для начала надо подключится к БД.
                //Тип 
                debitorDA.InsertCommand.CommandType = CommandType.Text;
                //Строка запроса с параметрами
                debitorDA.InsertCommand.CommandText = string.Format(
                    "INSERT INTO Debitors (ID, Name, PostNumber, PhoneNumber) VALUES (@ID, @Name, @PostNumber, @PhoneNumber);");    //Текст комманды на вставку 
                //Добавляем сами параметры. ИмяПараметра, Тип, размер, имяЯчейкиИзкоторойБрать
                debitorDA.InsertCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.UniqueIdentifier, 0, "ID")); //В первый @ID - указан в CommandText, помещаем значение из второго в DataRow (то которое хотим достать) из DataTable нашего DataSet'a... 
                debitorDA.InsertCommand.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar, 0, "Name"));
                debitorDA.InsertCommand.Parameters.Add(new SqlParameter("@PostNumber", SqlDbType.NVarChar, 0, "PostNumber"));
                debitorDA.InsertCommand.Parameters.Add(new SqlParameter("@PhoneNumber", SqlDbType.NVarChar, 0, "PhoneNumber"));

                //debitorDA.DeleteCommand;
                //debitorDA.UpdateCommand;
             */
             #endregion
            //#3.29 SqlCommoBuilder - Аналог DataADapter.
            creditDA = new SqlDataAdapter("SELECT * FROM Credits ORDER BY OpenDate", connectionString);
            paymentDA = new SqlDataAdapter("SELECT * FROM Payments ORDER BY PaymentDate", connectionString);

            debitorCB = new SqlCommandBuilder(debitorDA); //Вручную настроили только SELECT(обязательно)и получили debitorDA.InsertCommand. Передавая адаптер debitorDA в SqlCommandBuilder мы генерируем остальные команды которых недостает. debitorDA.Update/DeleteCommand.
            creditCB = new SqlCommandBuilder(creditDA); //Теперь не нужно каждую команду заполнять через параметры. Можно свободно Вставлять, обновлять, удалять.
            
            //paymentCB = new SqlCommandBuilder(paymentDA); //Теперь не работает, т.к. два запроса будет из-за Сложного АДАПТЕРА
            //Ручная комманда для ДатаАдаптера, которая будет работать с Insert для Payment, Update для Credit
            //paymentDA.InsertCommand = new SqlCommand();
            paymentDA.InsertCommand = new SqlCommand("SavePayment"); //1111 Имя процедуры!
            paymentDA.InsertCommand.Connection = new SqlConnection(connectionString);
            //paymentDA.InsertCommand.CommandType = CommandType.Text;
            paymentDA.InsertCommand.CommandType = CommandType.StoredProcedure; //1111 Тип Хранимая ПРОЦЕДУРА!

            #region ЭТО ТЕПЕРЬ БУДЕТ ХРАНИТСЯ В ХРАНИМКАХ НА СЕРВЕРЕ!
            /* 
         * paymentDA.InsertCommand.CommandText = String.Format("INSERT INTO Payments " +
                "(Payments.ID, Payments.CreditsID, Payments.Amount, Payments.PaymentDate) " +
                "VALUES (@ID, @CreditID, @Amount, @PaymentDate); " +
            "UPDATE Credits SET Credits.Balance = (Credits.Balance - @Amount) " +
            "WHERE Credits.ID = @CreditID;");
         * */
            #endregion
            paymentDA.InsertCommand.Parameters.Add(new SqlParameter("@ID", SqlDbType.UniqueIdentifier, 0, "ID"));
            paymentDA.InsertCommand.Parameters.Add(new SqlParameter("@CreditID", SqlDbType.UniqueIdentifier, 0, "CreditsID"));
            paymentDA.InsertCommand.Parameters.Add(new SqlParameter("@Amount", SqlDbType.Decimal, 0, "Amount"));
            paymentDA.InsertCommand.Parameters.Add(new SqlParameter("@PaymentDate", SqlDbType.DateTime, 0, "PaymentDate"));
        }
        
        //#4 метод который будет возвращать DataSet
        public DataSet GetDBData()
        {
            try
            {
                //Ссылка на адаптеры и метод Fill - чтобы заполнить
                debitorDA.Fill(bankDataSet, "debitorTable"); //вытягивает данные по //#Запрос и создает и заполняем ими таблицу "debitorTable"
                creditDA.Fill(bankDataSet, "creditTable"); //Устанавливает связть с БД, выполняетт SELECT, получает данные и сохраняет их в таблицу creditTable, которая войдет в bankDataSet.
                paymentDA.Fill(bankDataSet, "paymentTable");

                //Указываем первичные ключи  NewPAyment - Для поиска Родительской строки, чтобы искать по первичному ключу - Find()
                bankDataSet.Tables[0].PrimaryKey = new DataColumn[] { bankDataSet.Tables[0].Columns[0] };
                bankDataSet.Tables[0].Columns[0].Unique = true; //Уникальность

                bankDataSet.Tables[1].PrimaryKey = new DataColumn[] { bankDataSet.Tables[1].Columns[0] };
                bankDataSet.Tables[1].Columns[0].Unique = true; //Уникальность

                bankDataSet.Tables[2].PrimaryKey = new DataColumn[] { bankDataSet.Tables[2].Columns[0] };
                bankDataSet.Tables[2].Columns[0].Unique = true; //Уникальность

                //Связь #1
                    //#5 Relation - Создали Связь bankDR
                    DataRelation bankDR = new DataRelation("DebitorCredit", //Название связи
                        bankDataSet.Tables["debitorTable"].Columns["ID"],   //Внешний Ключ в Debitor имя колонки
                        bankDataSet.Tables["creditTable"].Columns["DebitorID"]);    //Внутренний Ключ в Credit
                    //#5.1 Добавить связь bankDR
                    bankDataSet.Relations.Add(bankDR);

                //Связь #2 Credit - Payment
                    bankDR = new DataRelation("CreditPayment",
                        bankDataSet.Tables["creditTable"].Columns["ID"],
                        bankDataSet.Tables["paymentTable"].Columns["CreditsID"]);
                    bankDataSet.Relations.Add(bankDR);
            }
            catch
            {

            }
            return bankDataSet; //Неизмененный DataSet, это тот который только что вытянули из БД 
        }
        internal bool SaveChanges(DataSet bankDataSet_)
        {
            try
            {
                //Update - взвращет тип int и говрит сколько было измененно записей на сервере(пока ненужно)
                debitorDA.Update(bankDataSet_.Tables["debitorTable"]);  //Нужно получить измененный клиентский DataSet, а не тот который еще не сохранен в методе #4 GetDBData() 
                creditDA.Update(bankDataSet_.Tables["creditTable"]);    //Порядок важен. creditDA завязан на debitorDA
                paymentDA.Update(bankDataSet_.Tables["paymentTable"]);
            }
            catch 
            {
                return false;
            }
            return true;
        }
        public bool SaveAllData(DataSet ds)
        {
            try
            {
                ds.WriteXml("BANK_data.xml");
                ds.WriteXmlSchema("BANK_data.xsd");
            }
            catch
            {
                return false;
            }

        return true;
        }
    }
}
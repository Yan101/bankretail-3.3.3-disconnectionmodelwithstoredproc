﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BANKretail
{
    //#2
    public partial class NewDebitor : Form
    {
        //Открытые поля, те которые будут видны из Гл формы, 
        //после того как эта форма установит для себя DialogResult(она закроет себя, но остентся в памяти).
        public Guid DebID
        {   
            get { return new Guid(txbx_debitorID.Text); }   
        }
        public string DebName
        {
            get { return txbx_debitorName.Text.Trim(); }
        }
        public string DebPostNum
        {
            get { return txbx_debitorPostNumber.Text.Trim(); }
        }
        public string DebPhoneNum
        {
            get { return txbx_phoneNumber.Text.Trim(); }
        }
        DAL dal = new DAL();
        
        public NewDebitor()
        {
            //Контролы создаются после этого метода!
            InitializeComponent();
            txbx_debitorID.Text = Guid.NewGuid().ToString(); //При открытии сразу генерируем ID
        }

        private void btn_saveNewDebitor_Click(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(txbx_debitorName.Text.Trim()) ||
                String.IsNullOrEmpty(txbx_debitorPostNumber.Text.Trim()))
                DialogResult = DialogResult.No; //Закроем форму
            else
                //DebNAme и DenPostNumber - обязательно заполнены. т.к. они notNull !!!
                DialogResult = DialogResult.OK; //Дебитор успешно внесен.
        }
    }
}

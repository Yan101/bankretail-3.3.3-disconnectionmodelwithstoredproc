﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Collections;
namespace BANKretail
{
    public partial class NewPayment : Form
    {
        DataView allCreditsDV; //#3 Фильтрованные данные для данного дебитора
        DataSet ds;
        public NewPayment(DataSet ds)
        {
            InitializeComponent();
            this.ds = ds;
            //#4 Заполняем ID для NewPayment
            PaymentID = Guid.NewGuid(); 
            txbx_paymentID.Text = PaymentID.ToString(); 
            
            //#5 Заполняем ЛистБоксы1 для Дебиторов
            ltbx_debitorID.DataSource = ds.Tables["debitorTable"];
            ltbx_debitorName.DataSource = ds.Tables["debitorTable"];

            //#6 Выбирать Кредиты для динамичеки выбираемых Дебиторов. 
            allCreditsDV = new DataView(ds.Tables["creditTable"]); //Филтруется в зависимости от выбранного пункта в ltbx_debitor
            ltbx_creditID.DataSource = allCreditsDV;
            ltbx_creditAmount.DataSource = allCreditsDV;
            ltbx_creditBalance.DataSource = allCreditsDV;
            //Обязательно подписатся на событие
        }

        //#2 СВОЙСТВА
        public Guid CreditID { get; private set; } //Только на чтение внутри класса
        public Guid PaymentID { get; private set; }
        public decimal PaymentAmount { get; private set; }
        public DateTime PaymentDate { get; private set; }

        //#1 МЕТОДЫ
        private void txbx_paymentAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Проверка на запятую!
            if (e.KeyChar == ',')
            {
                if (txbx_paymentAmount.Text.Trim().Contains(',') ||     //Нельзя вводить запятую второй раз! Если вся строка содержит запятую! 
                       txbx_paymentAmount.Text == String.Empty)         //Нельзя вводить запятую вначале, в пустое поле!  
                {
                    e.Handled = true; //не позволяем вводить символ. WTF???
                    return; //выйти из метода
                }
                else
                {
                    e.Handled = false;
                    return;
                }
            }

            //Если не запятая то надо проверить, что это числовой символ!
            short res;
            if (Int16.TryParse(e.KeyChar.ToString(), out res))
                e.Handled = false;
            else
                e.Handled = true;
        }
        private void txbx_paymentAmount_Leave(object sender, EventArgs e)
        {
            //проверка на пустое значение
            if (txbx_paymentAmount.Text.Trim() == String.Empty)
            {
                lbl_message.Text = "Сумма платежа не введена!";
                lbl_message.ForeColor = Color.Red;
                btn_saveNewPayment.Enabled = false;
                return; //чтобы не попадать в блок который НИЖЕ !! 
            }
            try
            {
                //Сумма платежа в рамках > 100$ и не больше чем есть на балансе!!!!
                decimal payValue = decimal.Parse(txbx_paymentAmount.Text.Trim());
                if (payValue < 100 ||
                    payValue > decimal.Parse(ltbx_creditBalance.SelectedValue.ToString())) //Выбранная текущая запись. SelectedValue. Получаем  
                {
                    lbl_message.Text = "Сумма платежа не валидна!";
                    lbl_message.ForeColor = Color.Red;
                    btn_saveNewPayment.Enabled = false;
                }
                else
                {
                    lbl_message.Text = "Сумма платежа валидна";
                    lbl_message.ForeColor = Color.Green;
                    btn_saveNewPayment.Enabled = true;
                }
            }
            catch { }
        }
        private void btn_refresh_Click(object sender, EventArgs e)
        {
            txbx_paymentAmount.Text = "";
        }
        //#6 При выборе в листбоксе будет фильтр работать. Два листбокса работают по одному DAtaSource - поэтому можно подписать только один листбокс
        private void ltbx_debitorID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ltbx_debitorName.SelectedIndex == -1) //Если не выбрано
                return;

            //1 вытянуть DatARow
            DataRow debitorRow=this.ds.Tables["debitorTable"].Rows[ltbx_debitorName.SelectedIndex];
            //2 отфильтровать данные по полю ID
            allCreditsDV.RowFilter = string.Format("DebitorID='{0}'", debitorRow["ID"].ToString());
        }
        //#7 Занесение данных! отправить значение формы ипринят ьего в MainForm!
        private void btn_saveNewPayment_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txbx_paymentAmount.Text.Trim()))
                DialogResult = DialogResult.No;
            else
            {
                CreditID = new Guid(ltbx_creditID.SelectedValue.ToString());
                PaymentAmount = decimal.Parse(txbx_paymentAmount.Text.Trim());
                PaymentDate = dtp_paymentPassDate.Value;
                DialogResult = DialogResult.OK;
            }
        }
    }
}
